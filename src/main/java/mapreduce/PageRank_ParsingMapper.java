package mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/*
 * Parsing Mapper: Parse Enwiki file
 */
public class PageRank_ParsingMapper extends Mapper<LongWritable, Text, Text, Text> {
	
		
	private Text Key = new Text();
	private Text Value = new Text();
	private String RevisionID = "";
		
	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
			
		
		StringTokenizer tokenizer = new StringTokenizer(value.toString(), "\n");
				
		
		while (tokenizer.hasMoreTokens()) {
			String line = tokenizer.nextToken();
			//There are some records which MAIN line separate by \tab instead of space
			//fix the bug Jeremiah	
			line = line.replaceAll("\t", " ");
			String[] lineDivision = line.split(" ");
			
			//if Line contains REVISION --> extract Article title and RevisionID
			if (lineDivision[0].equals("REVISION")) {			
				
				//set article ID and Revision ID
				Key.set(lineDivision[3].trim());
				RevisionID = (lineDivision[2].trim());
				
			}
			//if line extract with MAIN
			else if (lineDivision[0].equals("MAIN")) {	
				//each page will be separated by space " "			

				//If we have empty string for revision ID, we set the value to 1.  This is to deal with typo mistakes here.
				if(RevisionID.equals("")){
				RevisionID = "1";
				}
							

				else{
				//getting rid of "MAIN"
				String Links = String.join(" ",lineDivision).substring(4);
				
			
				Value.set(RevisionID+ Links);

				
				
			
				//export to reducer :
				// ArticleTitle 	RevisionID Outlink1 Outlink2...
				context.write(Key,Value);
					
				}
				
				
		}
			
			
		
	}
  }
}

