package mapreduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class SinglePartitioner extends Partitioner<Text, Text> {
	public int getPartition(Text key, Text value, int numPartitions) {
		//to make sure that empty string key bug get fixed			
		if (key.toString().equals("")){
			return 0;
		}
		int c = Character.toLowerCase(key.toString().charAt(0));
		if (c < 'a' || c > 'z')
		return numPartitions - 1;
		return (int)Math.floor((float)(numPartitions - 2) * (c-'a')/('z'-'a'));
	}
}
