package mapreduce;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;



import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/*
 * Reducer of parsing Job
 */
public class PageRank_ParsingReducer extends Reducer
	<Text, Text, Text, Text> 
  {
		
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		// The maximum Revision ID is stored in the variable MaximumrevisionID
		long MaximumRevisionID = -1;
		String Links[] = new String[0];

		for (Text value : values) {
			String[] ValueDivision = value.toString().split(" ");
			long CurrentRevisionID = Long.parseLong(ValueDivision[0]);
			// updates the outlinks of the key(article_id) to the most recent edition of it
			if (MaximumRevisionID < CurrentRevisionID) {
				Links = Arrays.copyOfRange(ValueDivision, 1, ValueDivision.length);
				MaximumRevisionID = CurrentRevisionID;
			}

		}

		HashSet<String> HashSetNoRedundantLinkstmp = new HashSet<String>();

		for (int k = 0; k < Links.length; k++) {
			HashSetNoRedundantLinkstmp.add(Links[k]);
		}

		// if one of the outlinks is the page itself, we eliminate it from the set
		String Key = key.toString();
		HashSetNoRedundantLinkstmp.remove(Key);

		String NoRedundantLinks = StringUtils.join(HashSetNoRedundantLinkstmp, " ");
		String Value =  PageRankMainClass.DEFAULT_RANKING + " " + NoRedundantLinks;

		context.write(key, new Text(Value));

	}

}

