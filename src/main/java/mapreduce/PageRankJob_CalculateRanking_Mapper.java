package mapreduce;

import java.io.IOException;
import java.util.Arrays;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PageRankJob_CalculateRanking_Mapper extends Mapper<LongWritable, Text, Text, Text> {

@Override	
protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

		// fix some bug. Some elements are separated with tabs instead of spaces. We put
		// space everywhere to make sure it doesn't affect the rest of the code
		String LineSpace = value.toString().replace('\t', ' ');
		String Line = "";
		StringTokenizer tokenizer = new StringTokenizer(LineSpace, "\n");

		while (tokenizer.hasMoreTokens()) {
			Line = tokenizer.nextToken();
			String[] Divisions = Line.split(" ");
			Text ArticleTitle = new Text();
			Text Outlinks = new Text();
			// Since the format is now : Key PageRank Outlink1 Outlink2...Outlinkn the
			// number of links will be given by the length of the Array Division - 2.
			int NumberOfLinks = Divisions.length - 2;
			// making sure we avoid empty string bug
			if (!Divisions[0].equals("")) {
				ArticleTitle.set(Divisions[0]);
				Outlinks.set(StringUtils.join(Arrays.copyOfRange(Divisions, 2, Divisions.length), " "));
				context.write(ArticleTitle, Outlinks);

				// we pass to the reducer through this for loop the PR(v)/L(v) for every
				// outlinks associated with the key aticle Title
				for (int k = 2; k < Divisions.length; k++) {
					// making sure we avoid empty string bug
					if (!Divisions[k].equals("")) {
						// we put the prefix 'div' here to make sure that the result of this devision
						// does't get interpreted in the reducer as an oulink with a numerical value,
						// for example, a year.
						context.write(new Text(Divisions[k]),
								new Text("div" + String.valueOf(Double.parseDouble(Divisions[1]) / NumberOfLinks)));
					}
				}
			}
		}

	}
}
