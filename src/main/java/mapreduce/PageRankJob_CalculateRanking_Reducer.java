package mapreduce;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
/*
 * PageRankJob2_REDUCER: Calculate Page Ranking
 */

import mapreduce.PageRankMainClass;
public class PageRankJob_CalculateRanking_Reducer 
extends Reducer<Text, Text, Text, Text> { 
	
	@Override
protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		Double OutlinkDivisionSum = 0.0;
		String Outlinks = "";
		for (Text value : values) {
			String[] Divisions = value.toString().split(" ");
			// recuperating the result of the division done in the previous mapper
			if (Divisions.length == 1 && !Divisions[0].equals("") && Divisions[0].length() > 4
					&& Divisions[0].substring(0, 3).equals("div")) {
				try {
					double OutlinkDivision = Double.parseDouble(Divisions[0].substring(3));
					OutlinkDivisionSum += OutlinkDivision;
				}
				// ift the value starts with 'div' and is followed by something else than
				// numbers, than we treat it as the beginning of the list of outlinks
				catch (NumberFormatException e) {
					Outlinks = StringUtils.join(Divisions, " ");
				}
			}
			// if the value doesn't start with div than we treat it as a list of outlinks
			else {
				Outlinks = StringUtils.join(Divisions, " ");
			}

		}
		// write down here the new PageRank and the list of oulinks in value so the
		// second mapreduce job can loop properly if there is many iterations
		Text Value = new Text(String.valueOf(1 - PageRankMainClass.DAMPING_FACTOR + PageRankMainClass.DAMPING_FACTOR * OutlinkDivisionSum) + " " + Outlinks);

		context.write(key, Value);
	}
}
