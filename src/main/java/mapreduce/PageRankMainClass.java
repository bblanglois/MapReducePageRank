package mapreduce;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/*
 * This is the main class of Page Ranking Program
 * 1st step: get input parameter: input file, output folder, number of iteration
 * 2nd step: call parsing job
 * 3rd step: call calculate ranking job
 * 4th step: outputing correctly the file
 */
public class PageRankMainClass {

	
	//configuration variable
	public static String IN_PATH = "";
	public static String OUT_PATH = "";
	public static String DEFAULT_RANKING = "1.0";
        public static Double DAMPING_FACTOR = 0.85;


 
    	public static int ITERATIONS = 2;
    
    	public static String LINKS_SEPARATOR = "|";
    	public static NumberFormat NF = new DecimalFormat("00");
    
	
	public static void main(String[] args) throws Exception {
		
		PageRankMainClass pageRank = new PageRankMainClass(); 
		
		//check parameter length
		if (args==null|| args.length!=3) {
			System.out.print("Missing parameter ! You have to input 3 parameter: Input file, Output File and Number of Iteration");
		}
		else {
			PageRankMainClass.IN_PATH = args[0];
			PageRankMainClass.OUT_PATH = args[1];
			PageRankMainClass.ITERATIONS = Integer.parseInt(args[2]);
		}
		

		String inPath = null;
		String lastOutPath = null;
		
		
        // delete output path if it exists already
        FileSystem fs = FileSystem.get(new Configuration());
        if (OUT_PATH!=null &&  fs.exists(new Path(PageRankMainClass.OUT_PATH)))
        	fs.delete(new Path(PageRankMainClass.OUT_PATH), true);
		
		//step 1: parsing text file
		System.out.println("Running Job 1 - Parsing Enwiki Test file ...");
		
		boolean isCompleted = pageRank.jobProcessFile(IN_PATH,OUT_PATH+ "/iteration00");
	    if (!isCompleted)
	    {
	    	System.exit(1);
	    }
	    
	    System.out.println("Running Job 2 - Calculate Page Ranking...");
	    
	    
	    for (int runs = 0; runs < ITERATIONS; runs++) {
	    	//path of last job
	    	inPath = OUT_PATH + "/iteration" + NF.format(runs);
	    
            lastOutPath = OUT_PATH + "/iteration" + NF.format(runs + 1);
            System.out.println("Running Job 2 [" + (runs + 1) + "/" + PageRankMainClass.ITERATIONS + "] (PageRank calculation) ...");
            isCompleted = pageRank.jobCalculatePageRank(inPath, lastOutPath);
            if (!isCompleted) {
                System.exit(1);
            }
	    }
	    
   
	    
	    System.out.println("Running Job 3 to export output ...");
        isCompleted = pageRank.jobExportOutPut(lastOutPath, OUT_PATH + "/FinalResult");
        if (!isCompleted)
        {
            System.exit(1);
        }
        
       
        
        System.out.println("HADOOP IS AMAZING AND PAINFULLLLLL !");
        System.exit(0);
	    
	    
	}
	/*
	 * This method will call 1st Hadoop Job: Processing File
	 * in: input file
	 * out: output folder
	 * 	
	 */
	public boolean jobProcessFile(String in, String out) throws IOException, 
     ClassNotFoundException, 
     InterruptedException {

		//create new job
		Job job = Job.getInstance(new Configuration(), "PageRankJob_ParsingFile");
		job.setJarByClass(PageRankMainClass.class);
		
		//input mapper
		FileInputFormat.setInputPaths(job, new Path(in));
		job.setMapperClass(PageRank_ParsingMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setPartitionerClass(SinglePartitioner.class);

		//out reducer
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
	    	job.setOutputValueClass(Text.class);
	    	job.setReducerClass(PageRank_ParsingReducer.class);
	    
	    //job.setNumReduceTasks(1);
	    
	    
		FileOutputFormat.setOutputPath(job,new Path(out));
		
		
		 return job.waitForCompletion(true);
}
	/*
	 * Calculate Page Ranking Job
	 */
	public boolean jobCalculatePageRank(String in,String out) throws IOException, 
    ClassNotFoundException,InterruptedException{
		 
		Job job = Job.getInstance(new Configuration(), "PageRankJob_CalculateRanking");
	    job.setJarByClass(PageRankMainClass.class);
	        
	      // input / mapper
	     FileInputFormat.setInputPaths(job, new Path(in));
	     job.setMapOutputKeyClass(Text.class);
	     job.setMapOutputValueClass(Text.class);
	     job.setMapperClass(PageRankJob_CalculateRanking_Mapper.class);
	     job.setPartitionerClass(SinglePartitioner.class);
	     FileOutputFormat.setOutputPath(job, new Path(out));
		
	     
	     FileOutputFormat.setOutputPath(job, new Path(out));
	     job.setOutputFormatClass(TextOutputFormat.class);
	     job.setOutputKeyClass(Text.class);
	     job.setOutputValueClass(Text.class);
	     job.setReducerClass(PageRankJob_CalculateRanking_Reducer.class);
	     
	     //job.setNumReduceTasks(1);

	     return job.waitForCompletion(true);
	}

	/*
	 * Format the output properly
	 */
	
	
	public boolean jobExportOutPut(String in,String out) throws IOException, 
    ClassNotFoundException,InterruptedException{
		 
		Job job = Job.getInstance(new Configuration(), "PageRankJob_ExportOutPut");
	    job.setJarByClass(PageRankMainClass.class);
	        
	      // input / mapper
	     FileInputFormat.setInputPaths(job, new Path(in));
	     job.setMapOutputKeyClass(Text.class);
	     job.setMapOutputValueClass(DoubleWritable.class);
	     job.setMapperClass(PageRank_FinalMapper.class);
	     FileOutputFormat.setOutputPath(job, new Path(out));
	     
	     FileOutputFormat.setOutputPath(job, new Path(out));
	  	     
	     //No need reducer task here
	     job.setNumReduceTasks(0);

	     return job.waitForCompletion(true);
	}
	
}
