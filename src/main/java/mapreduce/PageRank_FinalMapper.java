package mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
/*
 * FinalMapper: print out Article Title and Score in the final format
 */
public class PageRank_FinalMapper extends Mapper
<LongWritable, Text, Text, Text> {
    
    @Override
    public void map(LongWritable key, Text value, Context context)
    		throws IOException, InterruptedException {
        
        StringTokenizer tokenizer = new StringTokenizer(value.toString(), "\n");

	while (tokenizer.hasMoreTokens()){
	String line = tokenizer.nextToken();
	line = line.replaceAll("\t", " ");       
	String[] LineDivision = line.split(" ");
        
	Text Key = new Text(LineDivision[0]);
	Text Value = new Text(LineDivision[1]);
        context.write(Key,Value);
        }
        
    }


}
