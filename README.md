###### This work is licensed under a Creative Commons [Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)


This README.md file provides detailed description of the implementation of every mapper and reducer used in the task that will be later described.
To not loose yourself in the verbose, you can just read the short description also provided with every such classe to understand the fundamental 
functionning of the implementation.

## INTRODUCTION

The goal of this exercise is to get familiar with the design, implementation and
performance testing of Big Data crunching tasks using Hadoop/MapReduce. It requires to
design and implement algorithms for parsing, filtering, projecting, and transforming data, over a
relatively large dataset, executing your code on a shared Hadoop cluster.


The main task of this code is to implement a watered-down version of the [PageRank algorithm] (https://fr.wikipedia.org/wiki/PageRank) that deals with a sample of the parsed  version of 
the complete [Wikipedia edit history as of January 2008] (https://snap.stanford.edu/data/wiki-meta.html), a single large text file in a tagged multi-line format.  Each revision history record 
consists of 14 lines, each starting with a tag and containing a spacedelimited series of entries. More specifically,each record contains the 
following data/tags, one tag per line:



##### REVISION: revision metadata, consisting of:

* article_id: a large integer, uniquely identifying each page.
* rev_id: a large number uniquely identifying each revision.
* article_title: a string denoting the page’s title (and the last part of the URL of the
page).
* timestamp: the exact date and time of the revision, in ISO 8601 format; e.g., 13:45:00
UTC 30 September 2013 becomes 2013-09-12T13:45:00Z, where T separates the
date from the time part and Z denotes the time is in UTC.
* [ip:]username: the name of the user who performed the revision, or her DNS-resolved
IP address (e.g., ip:office.dcs.gla.ac.uk) if anonymous.
* user_id: a large number uniquely identifying the user who performed the revision, or
her IP address as above if anonymous.

##### CATEGORY: list of categories this page is assigned to.
##### IMAGE: list of images in the page, each listed as many times as it occurs.
##### MAIN, TALK, USER, USER_TALK, OTHER: cross-references to pages in other namespaces.
##### EXTERNAL: list of hyperlinks to pages outside Wikipedia.
##### TEMPLATE: list of all templates used by the page, each listed as many times as it occurs.
##### COMMENT: revision comments as entered by the revision author.
##### MINOR: a Boolean flag (0|1) denoting whether the edit was marked as minor by the author.
##### TEXTDATA: word count of revision's plain text.
##### An empty line, denoting the end of the current record. 

Exemple :  

    REVISION 4781981 72390319 Steven_Strogatz 2006-08-28T14:11:16Z SmackBot 433328  
    CATEGORY American_mathematicians  
    IMAGE  
    MAIN Boston_University MIT Harvard_University  
    Cornell_University  
    TALK  
    USER  
    USER_TALK  
    OTHER De:Steven_Strogatz Es:Steven_Strogatz  
    EXTERNAL http://www.edge.org/3rd_culture/bios/strogatz.html  
    TEMPLATE Cite_book Cite_book Cite_journal  
    COMMENT ISBN formatting &/or general fixes using  [[WP:AWB|AWB]]  
    MINOR 1  
    TEXTDATA 229  
    [empty line]




The algorithm to calculate the pagerank for this task is given by this formula :  

    PR(u)=0.15 + 0.85 * Sum(PR(v)/L(v)), ∀v: ∃(v,u) ∈S

where L(v) is the number of out-links of page v.


## ASSUMPTIONS

To orientate my approach to this task, I made several assumptions concerning the algorithm I would implement, 
and concerning the data we would use for that algortithm implementation.  Here are the main ones :


-I consider only the latest version of each article, so that the outdated version of it doesn't affect their PageRank score.

-The element of the REVISION line rev_id is a unique number, incremented for every new article edits.

-I consider that repeated outlink(s) in the MAIN tag section are not be taken into account.  We assume that many outlinks to the same page 
in the same article should be considered a single unique outlink to that page.

-I consider that outlink(s) in the MAIN tag section pointing to the article itself should not be considered as a significant outlink.  


## MAPREDEUCE JOBS


To accomplish this version of the task, our implementation uses 2 and half mapredeuce jobs :

#### 1st Job :
-first mapper class : PageRank\_ParsingMapper.java  
-first reducer class : PageRank\_ParsingReducer.java

#### 2nd Job :
-second mapper class : PageRankJob\_CalculateRanking\_Mapper.java  
-second reducer class : PageRankJob\_CalculateRanking\_Reducer.java

#### 3rd Job (only used to format the final output) :
-third mapper class : PageRank_FinalMapper.java

Let's describe the work done by each of those classes :


### PageRank\_ParsingMapper.java

#### SHORT DESCRITPTION :
This Mapper parse the initial Data input into a particular format so that the reducer of the first job can treat properly.  
It stores the article\_title as the key, the rev\_id and the outlinks of this version of this article as the value.

#### DETAILED DESCRITPTION :
1.  This class receives the input data in the format described in the introduction (14 tag-data lines).  With the use of StringTokenizer, 
it treats that data line by line.  
2.  First, for every line of that data, all the tabs characters are converted into space characters to make uniform the space format
of that line, thus avoiding cases of typo related to those characters.  
3.  The elements of that line are then split into a string array, the first element of that array being the tag of that line.  
4.  If the tag of the treated line corresponds to "REVISION", we set the Text variable "Key" to the third element of that line 
(article\_title) and the second (rev\_id) in the String variable "RevisionID".  
5.  If the tag of the treated line corresponds to "MAIN",  we store in a String variable "Links" the string corresponding to a 
spcace character followed by all the outlinks of the currently treated article.  
6.  We set the Text variable named "Value" to the concatenation of the String variables "RevisionID" and "Links".
7.  We then context.write our Text variable "Key" as the key, and our Text Variable "Value" as the value so we get that output format :

for :  

     key	value
  
we get :  

    article_title		rev_id outlink1 outlink1 outlink2 outlink3 outlink4 outlink4 outlink5...  


### PageRank_ParsingReducer.java

This class receives the data from the previous mapper PageRank\_ParsingMapper.java in this format : 

for : 

    key	value  
we get :  

    article_title1 rev_id1 outlink1 outlink1 outlink2 outlink3 
    article_title2 rev_id2 outlink4 outlink1 outlink5...

#### SHORT DESCRITPTION :
This reducer allows us to remove every outdated article by keeping the most recent one for every unique article_title key.  
It also permits the removal of all redundant outlinks, all outlinks pointing to the article itself, for this up to date article version.

#### DETAILED DESCRITPTION :
1.  We set the initial maximum of the revision ID at 1 for every unique key treated. We store that value in a long variable 
named "MaximumRevisionID".
2.  We store an empty String array for every unique key treated in the variable "Links".  This variable will later store all the 
most up to date outlinks for ever up to date articles.
3.  We iterate in a for loop on every values of the unique treated key.
4.  The treated value in the for loop, separated by space character, is divided into a String array named ValueDivision.
5.  The rev_id of the treated value is parsed as a long and is stored in a long variable named CurrentRevisionID.
6.  We then compare that rev\_id to the MaximumRevisionID to make sure that we only keep the most recent version of the unique article\_title key treated.  The most recent version of this unique article\_title key, will contain in its value the highest long value 
of rev\_id, since every rev\_id is a unique number, incremented for every new article edit.
7.  If the CurrentRevisionID is geater than the MaximumRevisionID, then the MaximumRevisionID long value is updated to the 
CurrentRevisionID long value. The outlinks of the value presently treated are stored in the previously defined String Array "Links".
8.  Every different value corresponding to every unique key will go through that for loop to make sure that we only keep the most up 
to date version of each articles outlinks after this step.
9.  We create a HashSet<String> that we store in the variable "HashSetNoRedundantLinkstmp".
10.  We add to that set all of the outlinks of the most up to date version of the unique article_title key treated.  
11.  The mechanism of the HashSet<String> Object will allow us to eliminate evey redundant outlinks contained in it.
12.  We then store the string value of our article\_title unique key in a variable named "Key" and if such a string is 
contained in the set, we remove it. This is to make sure that every potential outlink for that unique article_title key pointing 
to the article itself is removed.
13.  All the strings contained in the "HashSetNoRedundantLinkstmp" Hashset are then reparsed in a string, every one of them separated by a space
character.  They are stored in the String variable NoRedundantLinks. 
14.  The PageRankMainClass.DEFAULT_RANKING static constant string variable representing the initial given default PageRank (set at "1.0" here)
and the NoRedundantLinks String are then stored in the String Variable Value.
15.  We then context.write the currently treated Text variable key as the key, and the Text Parsed String varibale Value, as the value.
We get that format of output :

for :  

    key	value  

we get : 
 
     article_title		PageRank outlink1 outlink2 outlink3 outlink4 outlink5...  


### PageRankJob\_CalculateRanking\_Mapper.java  


This class receives the data from the first job reducer PageRank\_ParsingReducer.java or the second job reducer 
PageRankJob\_CalculateRanking\_Reducer.java in this format :

for :  

    key   value    

we get :  

    article_title		PageRank outlink1 outlink2 outlink3 outlink4 outlink5...


#### SHORT DESCRITPTION :
Assuming the full PageRank algorithm is PR(u)=0.15 + 0.85 * Sum(PR(v)/L(v)), ∀v: ∃(v,u) ∈S, where L(v) is the number of out-links of page v, 
this mapper allows us to write for values the PR(v)/L(v) for every articles pointing to key u.  We will have after this mapper many <key,value> 
with the same key, since there can be many links pointing to key u. It also allows us to write all the outlinks for every unique key.  We need 
to keep that information for eventual iterations, wich consists of looping the second mapredeuce job.  The reducer in the second job will sum 
up all these values (division results) for every unique key and implement all the other steps of the PageRank Algorithm (see next section).

#### DETAILED DESCRITPTION :
1.  First, for every line of that received data, all the tabs characters are converted into space characters to make uniform the space 
format of that line, thus avoiding cases of typo related to those characters (we had some bugs at that point without that mechanism). 
2.  We then use a StringTokenizer to treat the received data from the last reducer as lines.  Indeed, all key-value element is represented 
by a line in the input data file that this mapper receives from the previous reducer.
3.  The String value of the current line Token is contained into the string variable "Line".
4.  We then divide this String in a String array that we store in the variable "Divisions".
5.  We then create two Text variables, "ArticleTitle" and "Outlinks" where we will later store indeed the article_title, 
and the outlinks of that article.
6.  We store in the int variable "NumberOfLinks" the number of outlinks for the currently treated article.  We can caclculate that 
number by substracting 2 to the total length of the String array "Divisions" since its first value represents the article_title, its second the 
PageRank, and all the others, outlinks.
7.  We then store the article_title in the Text variable ArticleTitle.
8.  We set our Text variable "Outlinks" to the String value of all the "Divisions" String array elements joined together from third to 
the last element and all separated by space character.  These elements all represent outlinks for the currently treated article.  If an article 
doesn't have any outlinks, then we set to an empty string our Text variable "Outlinks".
9. We then context.write the article_title contained in the "ArticleTitle" as the key and the outlinks contained in the variable "Values" 
as the value.
10.  We then have a for loop wich will go through all the outlinks of an article (if there is).
11.  For each of those outlinks, we will context.write the current outlink treated in the loop as the key, and the result of the division 
of the PageRank of the aricle that point to that outlink by the number of outlinks of this same article, as value 
(note that this result will be parsed as a String and that it will be prefixed by the String "div"; this is to make sure that 
the next reducer doesn't interprete an outlink with a numerical value, a year for exemple, with a result of the previous mentionned division).  
This step allows us to stuck the String equivalent of  this part of the PageRank algorithm : PR(v)/L(v) where v is every article pointing to 
page u (see the formula in the introduction of that README file). We get that format of output :

for :   

    key	value  
    key	value  
    key	value  
    key	value	

Assuming the PageRank is 1.0 for all links at that point,


we get :  

     King		queen_Elizabeth Pince_Charles Buckingham_Palace United_Kingdom  
	 queen_Elizabeth	div0.25  
	 Pince_Charles		div0.25  	
	 Buckingham_Palace	div0.25  
	 United_Kingdom		div0.25  


### PageRankJob\_CalculateRanking\_Reducer.java


This class receives the data from the previous reducer PageRankJob_CalculateRanking_Mapper.java in this format :

for :  

    key	value  
    key	value  
    key	value  
    key	value  	

Assuming the PageRank is 1.0 for all links at that point,


we get :

     King		queen_Elizabeth Pince_Charles Buckingham_Palace United\_Kingdom  
	 queen_Elizabeth	div0.25  
	 Pince_Charles		div0.25  	
	 Buckingham_Palace	div0.25  
	 United_Kingdom		div0.25  
or :

	 article_title	outlink1 outlink2 outlink3
	 outlink1	divPR(article_title)/L(article_title)
	 outlink2	divPR(article_title)/L(article_title)
	 outlink3	divPR(article_title)/L(article_title)	

#### SHORT DESCRIPTION :
Assuming the full PageRank algorithm is PR(u)=0.15 + 0.85 * Sum(PR(v)/L(v)), ∀v: ∃(v,u) ∈S, where L(v) is the number of out-links of page v, 
if this reducer recognize the value as a result of this the division(PR(v)/L(v)), it sums up every (PR(v)/L(v)) for each unique corresponding key, 
mulitply it by the damping factor and adds it to 1 - the damping factor. This value is the new PageRank for every unique key.  If the 
reducer doesn't recognize the value as a division result, than the value is the outlinks of the corresponding key. Thus, this reducer 
allows us to finalize the calculation of the new PageRank of each unique key and to pass along this new PageRank and the outlinks of each 
unique key to the mapper of the second job in case of a multi-iterations request.

#### DETAILED DESCRITPTION :
1.  First, we store in the Double variable "OutlinkDivisionSum" the initial value of the Sum(PR(v)/L(v)) for every unique key. 
We set that initial value at 0.0.
2.  We create an empty String "Outlinks" that will contain the outlinks for every unique key that we need to pass again to second job 
mapper in case of multi-iterations request.
3.  We iterate through all the values for every unique key by creating a for loop.
4.  We divide in a String array "Divisions" all the elements of the value iterated. 
5.  We parse the value wich consist of the result of the division (PR(v)/L(v)) calculated in the previous mapper if length of the 
array "Divisions" is 1, if the element of that array is more than 4, and if the prefix "div" is the first three character of the key.  
This mechanism allows the reducer the differentiate a value wich consist of the result of the division precedently mentionned, from a value 
wich would consist of the list of outlinks that we need to keep for every article.  If the value is recognized as a division result, that 
result is parsed in a Double value.  Every parsed values is then summed up for each corresponding unique key, like the PageRank algorithm 
suggests.
6.  If  the value is not recognized as division result, then its value represents the outlinks for its unique key.  This value is than 
stored in the String variable "Outlinks".
7. We then store in the Text variable Value the new PageRank for every article, using the PageRank algorithm, a space character, 
and the outlinks corresponding to that unique key (the article_title).
8. We then context.write the article_title as the keyand the Text variable Value as the value. We get that format of output :

for :  

     key	value
     key	value
     key	value

Assuming that the article red and green point to no outlinks , and that blue points to outlinks green and red

we get :  

    red		0.26177772377239875
    green		0.38948888482187254
    blue		0.57388883882983645 green red	

			


### PageRankJob_FinalMapper.java


This class receives the data from the previous reducer PageRankJob_CalculateRanking_Reducer.java in this format :

for :  
  
    key     value  
    key	value  
    key	value

Assuming that the article red and green point to no outlinks , and that blue points to outlinks green and red

we get :  

    red		0.26177772377239875
    green		0.38948888482187254
    blue		0.57388883882983645 green red

	

#### SHORT DESCRIPTION :
This mapper format properly the final output, after all iterations completed.

#### DETAILED DESCRIPTION :
1.  With the use of StringTokenizer, the received data is treated line by line.
2.  We replace every tab charactr for space character in that line to avoid spacing bugs.
3.  The line is then divided into a String Array named "LineDivision".
4.  Since we only need the article_id and the final PageRank of each article as a final output, we store in a Text variable "Key", 
the article_id, wich is the first element of "LineDivision" and the Pagerank, wich is the second element of "LineDivision", in a Text 
Variable "Value".
5.  We context.write the Text variable "Key" as the key and the Text variable "Value" as the value. We get this format of the output

2008\_Alabama\_Crimson\_Tide\_football\_team	0.15000000000000002  
3-4	0.16593750000000002


## JUSTIFICATION
Note that this section can varry depending on the type and the toponomy of the cluster on which the mapreduce job is executed.

#### Handling data errors: This is a big data file so there are many cases that we need to handle:
  
-One article title can be appeared many times --> We use revisionID to get the latest version of an article title.     
-In the list elements of MAIN tag, there could be duplicated outlinks --> We use HashSet to store outlinks in order to remove duplication.    
-Empty string bug --> We check empty string before processing some thing.  
-Inconsistent delimeter (There are some records which MAIN line separated by \tab instead of space). --> We replace all "\t" by space before processing.

#### Improving performance:  
   
-Using Custom Partitioner class.  
-For the third job which exports only data , we don't use reducer class to increase performance.  
-Removing duplication outlinks in the MAIN tag and keeping only updated version of an article title before calculating page rank so that we can increase the processing time.

#### Easy debugging :  
 -We keep separate folders for each iteration so that user can easily check the input/output for debugging.  
    
        bash-4.2$ hdfs dfs -ls /user/2300821l/PageRank
        Found 4 items
        drwxr-xr-x   - 2300821l hadoop          0 2018-02-20 12:37 
        /user/2300821l/PageRank/FinalResult  
        drwxr-xr-x   - 2300821l hadoop          0 2018-02-20 12:35 
        /user/2300821l/PageRank/iteration00  
        drwxr-xr-x   - 2300821l hadoop          0 2018-02-20 12:36 
        /user/2300821l/PageRank/iteration01  
        drwxr-xr-x   - 2300821l hadoop          0 2018-02-20 12:37 
        /user/2300821l/PageRank/iteration02

 
By Benjamin B. Langlois 
2018
